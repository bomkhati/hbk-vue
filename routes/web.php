<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('product', [\App\Http\Controllers\Product\ProductController::class, 'create'])->name('product');
Route::post('product', [\App\Http\Controllers\Product\ProductController::class, 'store'])->name('product.create');
Route::get('product/create', [\App\Http\Controllers\Product\ProductController::class, 'create'])->name('products.make');
//Route::get('product/store', [\App\Http\Controllers\Product\ProductController::class, 'store'])->name('product.store');
Route::get('product/view', [\App\Http\Controllers\Product\ProductController::class, 'index'])->name('products.view');
