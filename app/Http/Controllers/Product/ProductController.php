<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Modules\Services\Product\ProductService;
use App\Http\Requests\Product\ProductRequest;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $product;
    function __construct(ProductService $product)
    {
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $products  = $this->product->all();
        return view('product.index',compact('products'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('demo.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->product->create($request->all())) {
            return response(['status' => "OK"], 200);
        }

        return response(['status' => "ERROR"], 500);
    }


}
