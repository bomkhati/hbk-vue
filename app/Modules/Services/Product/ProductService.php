<?php namespace App\Modules\Services\Product;

use App\Modules\Models\Banner\Banner;
use App\Modules\Models\Product\Product;
use App\Modules\Services\Service;

class ProductService extends Service
{
    protected $product;

    public function __construct( Product $product)
    {
        $this->product  = $product;

    }


    /**
     * Create new Session
     *
     * @param array $data
     * @return Session|null
     */

    public function create(array $data)
    {
        try {
            $product = $this->product->create($data);
            return $product;
        } catch (Exception $e) {
            return null;
        }
    }


    /**
     * Paginate all Session
     *
     * @param array $filter
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 5;

        return $this->product->paginate($filter['limit']);

    }

    /**
     * Get all Session
     *
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function all()
    {
        return $this->product->get();

    }

    /**
     * Get a Session
     *
     * @param $productId
     * @return void
     */
    public function find($productId)
    {

    }

}
