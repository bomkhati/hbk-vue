<?php namespace App\Modules\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{


    protected $fillable = [
        'category', 'title', 'price', 'is_public'
    ];

}
