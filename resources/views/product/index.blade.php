@extends('layout')

@section('content')

    <div class="container-fluid">
        @include('layouts.flash-message')
        <div class="row products">

            @foreach($products as $product)
                <div class="col-md-3">
                    <div class="thumbnail">
                        <img src="{{ $product->image }}" class="img-thumbnail" alt="">
                        <div class="caption text-center">
                            <h4>{{ $product->name }}</h4>
                            <p><strong>Price: </strong> ${{ $product->price }}</p>
                            <p class="btn-holder">
                                <a href="{{route('add-to-cart',$product->slug)}}" class="btn btn-warning btn-block text-center" role="button">Add to cart</a>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
