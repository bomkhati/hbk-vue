import apiService from '../../api-service';

export default class VenueService {
    #api = null;

    constructor() {
        this.#api = '/product';
    }
    paginate(data={},index = null) {
        let url = `${this.#api}`;
        if (index != null)
            url = `${url}?page=${index}`;
        let param ={
            params: data
        }
        return apiService.query(url,param);
    }

    update(id,data) {
        let url = `${this.#api}/${id}`;
        return apiService.post(url,data)
    }

    show(venueId){
        let url = `${this.#api}/${venueId}`;
        return apiService.get(url)
    }

    create(data) {
        let url = `${this.#api}`;
        return apiService.post(url, data);
    }

    delete(id) {
        let url = `${this.#api}/${id}`
        return apiService.delete(url);
    }

}